
state={
	id=184
	name="STATE_184"
	manpower = 382500
	
	state_category = rural
	
	resources={
		chromium=16
		steel = 20
	}

	history={
		owner = GRE
		victory_points = {
			9930 1 
		}		
		buildings = {
			infrastructure = 3
		}
		add_core_of = GRE

	}

	provinces={
		6990 9791 9930 11774 11905 
	}
}
