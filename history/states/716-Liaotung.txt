state={
	id=716
	name="STATE_716"
	resources={
		steel=21.000
	}

	history={
		owner = JAP
		buildings = {
			infrastructure = 6
			arms_factory = 3
			industrial_complex = 3
			air_base = 5
			964 = {
				naval_base = 3

			}

		}
		victory_points = {
			11771 3 
		}

	}

	provinces={
		964 3783 3895 3970 6804 6951 6981 9835 9885 11764 11771
	}
	manpower=10264620
	buildings_max_level_factor=1.000
	state_category=city
}
