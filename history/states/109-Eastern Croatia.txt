
state={
	id=109
	name="STATE_109" # Croatia
	manpower = 3224400
	
	state_category = large_town

	history={
		owner = YUG
		buildings = {
			infrastructure = 4
			industrial_complex = 2			
		}
		victory_points = {
			11581 3 
		}
		victory_points = {
			11901 5 
		}
		victory_points = {
			3601 5 
		}
		victory_points = {
			6647 5 
		}
		victory_points = {
			624 5 
		}
		add_core_of = YUG
	}

	provinces={
		624 3592 3596 3627 6647 9595 9611 11577 11581 11594 3601 591 11901 6611 984 
	}
}
